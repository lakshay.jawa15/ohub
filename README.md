 
# NORA Re-Sourcing / Re-Routing to a different Store or DC
## Service Wise Changes
### Inventory Service
-  Reservation API will be changed to delete the inventory for a UPC from the store which is cancelling the item
- Reservation API call will be done with `Distro_ALL` so that orders are re-routed as per sourcing rules
- The reservation API will be enhanced such that the UPC to be deleted can be highlighted in the request.
- Json for reservation API request is below: 
```json5
{
  "orderId": 898926767,
  "upcList": [
    {
      "upc": "190849672275",
      "quantity": 5,
      "deleteInventory": true
      // This is the new field being added default value will be false
    }
  ]
  // ... rest of the fields have been skipped
}
```

- There will be queryParam used in the API to identify if it is re-route flow similar to virtual orders:
    For e.g., `{{HOST}}/api/v2/inventory/reservation?reroute=true`
  
   - Assuming that reroute=true and orderId is populated and deleteInventory is true for any UPC the inventory would be marked as 0 in the database.
    The store will be identified by querying the `reservation` and `reservation_line_item` table in the database.
    - Default value of reroute query param will be false
    - The orderId will be same as before in case of full reroute. 
    - OrderId will be suffixed/prefixed with split_id in case of partial reroute.
    - Or add a new `split_id` column in reservation table and the api.
    - if a store is canceling the order then we should exclude that store during reservation to avoid re-routing back to it. 
      Remove all the stores to whom this order has been assigned earlier. 101

### Sales Order Service

#### Cancellation Reason

 - Backed API for rest endpoint for get,get all, put ,delete for cancellation reason entity.

#### Order status movements
- No Duplicates will be allowed in the order statuses (No Change this is existing functionality)
- New statuses added are `ReRoutedToSTR` ,`ReRoutedToDC`

```mermaid
stateDiagram-v2
    [*] --> Created
    Created --> PickUpReady
    Created --> ReRoutedToSTR
    ReRoutedToSTR --> ReRoutedToSTR
    ReRoutedToSTR --> ReRoutedToDC
    ReRoutedToDC --> [*]
    ReRoutedToSTR --> PickUpReady 
    PickUpReady --> Completed
    Completed --> [*]
    ReRoutedToSTR --> Cancelled
    Cancelled --> [*]
```
#### Line Items status movements
- Duplicates are allowed in the line items statuses (No Change this is existing functionality)
- New statues added are : 

```mermaid
stateDiagram-v2
    [*] --> Created
    Created --> PickUpReady
    Created --> Cancelled
    Cancelled --> ReRoutedToSTR
    ReRoutedToSTR --> Cancelled
    ReRoutedToSTR --> ReRoutedToDC
    ReRoutedToDC --> [*]
    Cancelled --> [*]
    ReRoutedToSTR --> PickUpReady
    PickUpReady --> PartiallyProcessed 
    PickUpReady --> Completed
    PartiallyProcessed --> [*]
    Completed --> [*]
```

#### Scenarios for Rerouting and Splitting order internally
- Scenario 1 Normal Flow
```mermaid
sequenceDiagram
    participant Created
    participant PickUpReady
    participant Completed
    Created->>PickUpReady: 1
    activate PickUpReady
    Note over Created: Normal Flow
    PickUpReady->>Completed: 2
    deactivate PickUpReady
```

- Scenario 2 Multiple Rerouting without creating new order (in case where full order / all line items canceled)
    - In this case all line items(UPCs) should be available at the same store 
    
```mermaid
sequenceDiagram
    participant Created
    participant PickUpReady
    participant ReRoutedToSTR
    participant Completed
    Created->>ReRoutedToSTR: 1
    activate ReRoutedToSTR
    Note over Created: Store A user Cancels full Order
    Note right of ReRoutedToSTR: Order is ReRouted
    loop Max Number Of ReRoutes = 3
      ReRoutedToSTR ->> ReRoutedToSTR: Assigned To Store B <br>Store B cancels the order <br> Assigned To Store C <br>Store C cancels the order <br>Assigned To Store D
    end
    ReRoutedToSTR->>PickUpReady: 2
    deactivate ReRoutedToSTR
    activate PickUpReady
    Note left of PickUpReady: Store D fulfills order
    PickUpReady->>Completed: 3
    deactivate PickUpReady
```

- Scenario 3 Re-Route to DC
  - In this case all line items(UPCs) should be available at the same store / DC
    
```mermaid
sequenceDiagram
    participant Created
    participant PickUpReady
    participant ReRoutedToSTR
    participant ReRoutedToDC
    Created->>ReRoutedToSTR: 1
    activate ReRoutedToSTR
    Note over Created: Store A user Cancels full Order
    Note right of ReRoutedToSTR: Order is ReRouted
    loop Max Number Of ReRoutes = 3
      ReRoutedToSTR ->> ReRoutedToSTR: 2. Assigned To Store B <br>Store B cancels the order <br> 3. Assigned To Store C <br> Store C cancels the order <br> 4. Assigned To Store D<br> Store D Cancels the Order
    end
    deactivate ReRoutedToSTR
    Note right of ReRoutedToDC: A new Order status message <br> is triggered to WCS to release a new <br> Order to DC
    alt DC Availability
       ReRoutedToSTR->>ReRoutedToDC: 5
    else Not Available at DC
      ReRoutedToSTR->>Cancelled: 5 
    end
```

- Re Route an order in case of partial fulfillment by a store
  - Case where there are multiple line items in an order and one of them is canceled.
  - The cancelled items should have shipped quantity as zero.
  
```mermaid
sequenceDiagram
    participant Created
    participant PickUpReady
    participant Completed
    participant ReRoutedToSTR
    participant Cancelled
    Created->>PickUpReady: 1
    Note over Created: Order with Multiple Line Items
    PickUpReady->>Completed: 2
    Note over PickUpReady: One Line Item is fulfilled <br> Other Line item(s) is/are cancelled
    Created->> ReRoutedToSTR: 3
    Note over Created: One or Multiple new <br> internal orders maybe <br>created for cancelled items
    Note right of ReRoutedToSTR: Order is ReRouted
    loop Max Number Of ReRoutes = 3
      ReRoutedToSTR ->> ReRoutedToSTR: 2. Assigned To Store B <br>Store B cancels the order <br> 3. Assigned To Store C <br> Store C cancels the order <br> 4. Assigned To Store D
    end 
    alt Store D Fulfils the Order
       ReRoutedToSTR->>PickUpReady: 4
       PickUpReady->>Completed: 5
    else Store D Cancels the Order
      ReRoutedToSTR->>Cancelled: 4
    end
    Note right of Cancelled: Order Status update<br> is sent to WCS for <br>line items in internal order
```

#### ReRouting / Splitting Logic

1. Rest API for Order status update gets triggered from UI by the store user.
2. Enhance the API for Cancel/ PickUpReady status to check the cancellation reason for unfulfilled line items.
3. After you find line items with shipped quantity 0, make an entry in the `sales_order_on_hold` table for further processing.
4. Do not publish eventuate events for the orders until the cancelled items until rerouted or cancellation is complete for the order.
5. Order status will be moved to state based on user action to ensure no change in user experience.   
6. A new scheduler polls the `sales_order_on_hold` table and makes an API call to inventory service to reserve inventory.
   For each order, do the following: 
    1. Check the `reroute_count` of the order
        1. if reroute_count >= maximum configured store reroutes then:
            1. Trigger the Completed status event, in case of partial processing.
            2. Trigger the Cancelled status event, in case of full cancellation.
        2. if reroute_count < maximum configured store reroutes then:
            1. If the reservation fails, then
                1. Trigger the Completed status event, in case of partial processing.
                2. Trigger the Cancelled status event, in case of full cancellation.
            2. If the reservation is successful, [REVISIT: Ratio is to be discussed with business]
                1. Count the items to be shipped from DC and Stores.
                2. If the ratio of STORE/DC >= 0.5 then ReRoute the order to Stores, otherwise reroute the order to DC
                3. If order is an internal order (check if split_id is not null)
                    1. if all items are available at the same store, then reroute to store
                    2. if all items are NOT available at the same store, then cancel the order. 
                       [REVISIT: We may assign at least one item to store]
                       [REVISIT: We may need to cancel this reservation, to revert inventory]
                       [REVISIT: Implement an API to cancel the reservation in inventory service]
                       (No further split now)
               4. If order is NOT an internal order, 
                  1. if all items are available at the same store, then reroute to store
                  2. if all items are NOT available at the same store, then create internal orders for each store.
 
        3. Manipulate the `sales_order_status` table to maintain the most recent status correctly.
           1. Delete the current status and update the new status as Rerouted, if order got rerouted and current status = Cancelled.
        4.  Publish any necessary domain events after the above steps        
        5. Delete the entry from `sales_order_on_hold` once all the work is complete.
           Publish any necessary domain events after the above steps
 

#### Database Changes for Internal Split Orders
- New fields will be added in the sales order table `split_id` , `reroute_count`
- Existing unique constraint on `application_order_id` will be dropped
- A new unique constraint on combination of fields `application_order_id` & `split_id` will be added.
- Audit table for rerouting for all line items `reroute_history`.
- `split_id` to be added in `shipping_label_details` as well
- a new table for `cancellation_reason`
 
Examples shown below: 

- sales order table

sales_order_id | application_order_id | split_id | reroute_count | 
-------------- | -------------------- | -------- | ------------- |
1          | 101                  |   null   |       0       |
2          | 201                  |   null   |       0       |
3          | 201                  |   1      |       1       |
4          | 301                  |   null   |       0       |
5          | 301                  |   1      |       1       |
6          | 301                  |   2      |       3       |

- shipping details table
  
id  | application_order_id | split_id | tracking_number |
----| -------------------- | -------- | --------------- |
1   | 101                  |   null   |  1111111111     |
2   | 301                  |   null   |  2222222222     |
5   | 301                  |   1      |  3333333333     |
6   | 301                  |   2      |  4444444444     |

- reroute_history table

id  |   sales_order_line_item_id | from_source | to_source |    created_on   |   modified_on   |
--- |  ------------------------ | ----------- | --------- | --------------- | --------------- | 
1   |   1011                    |  280        |  257      | 5/5/21 00:00:00 | 5/5/21 00:00:00 |
1   |   1011                    |  280        |  252      | 5/5/21 00:01:00 | 5/5/21 00:01:00 |
1   |   1011                    |  252        |  120      | 5/5/21 00:02:00 | 5/5/21 00:02:00 |
1   |   1011                    |  120        |  1007     | 5/5/21 00:03:00 | 5/5/21 00:03:00 |

- cancellation reason 

id  | reason_code            | delete_inventory   | reason_description
--- | --------------------- |------------------- | ------------------ 
1   | OutOfStock            | Yes                | Out of Stock
2   | Defective             | Yes                | Items are Defective
3   | ShipNotPossible       | No                 | Shipping Not Possible
4   | CarrierRefused        | No                 | Carrier Refused Pickup


### SalesOrder Pub Service

- A new order status message needs to be published for `ReRoutedToDC` status when the order rerouted to DC.
- `ReRoutedToSTR` is internal to OrderHub so no change required. The store which is updated is already sent in `PickUpReady`/`Completed` status message

### Notification Service

- Need to re-trigger an email whenever an order is re-routed to a new store.
- A new Kafka Consumer for a new order status for example, `ReRoutedToStoreDomainEventConsumer` will make calls to existing methods to leverage email sending functionality

### Frontend Changes
- Full Order Cancellation
    - Pop-up will be shown on click of cancel order button, where the store associate will need to fill the cancellation reason
    from the list of reasons for each line item.
    - Should we show cancellation reason for BOPIS / ROBIS? If yes then do we clear the inventory for the items cancelled?      
  
- Partial Order Cancellation 
    - In this case the store user would fulfill one line item (maybe partially) .
    - The second line item would be canceled which means that shipped quantity will be marked as zero for the second item. 
    - In this case the existing PrepareOrder popup would have a dropdown for the cancellation reason which store user will select
    This canceled item maybe rerouted later to other store as per the rules.
  
- A new page for CRUD operations for Cancellation Reason
    - A new page visible only to admin role so that they can create/update the cancellation reasons for the orders.

- Order Detail Page Changes
    - On order detail page we need to show few statuses in the timeline section.
    - The new statuses will be: `ReRoutedToSTR` , `ReRoutedToDC`
    - On the order detail page, we need to show the new order id, number of times an order has been rerouted etc. 
    - Also need to show an indicator that if the order is an internally split order or not.
    - Need to make sure that All actions on `Created` status are available for `ReRoutedToSTR` status.
    - No action should be available for `ReRoutedToDC` status, it should be treated as `Completed` status.


    
